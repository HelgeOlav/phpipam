package phpipam

import "reflect"

// ObjectMetadata returns some metadata about an object that can be used to work with phpIPAM.
// Look into tag.go to see a simple use case.
// BasePath has to be the path to the controller that works on some kind of data in phpIPAM.
// GET to BasePath should return all objects of that type.
// GET to BasePath/{id} should return a specific object.
// POST to BasePath should create a new object.
// PATCH to BasePath/{id} should update a specific object. (Can be changed with PatchOnId.)
// DELETE to BasePath/{id} should delete a specific object. (Can be changed with DeleteOnId.)
// Not all controllers / objects supports all methods above. In that case the call will just fail.
type ObjectMetadata struct {
	BasePath   string // base path to controller
	ReadOnly   bool   // true if you only can read from the controller
	PatchOnId  bool   // true if controller PATCH requests should go to BasePath/{id} instead of BasePath
	DeleteOnId bool   // true if controller DELETE requests should go to BasePath/{id} instead of BasePath
}

// ManagedObject any struct that implements this interface can be used with Connection.*Object functions.
// Most API calls to phpIPAM follows the same pattern and as long as that pattern is followed this interface will work.
type ManagedObject interface {
	GetObjectMetaData() ObjectMetadata
	GetObjectID() string
}

// GetObjectMetadata takes an input of either a object or an array of the objects
// and returns true if it has implemented ManagedObject.
func GetObjectMetadata(input interface{}) (bool, ObjectMetadata) {
	if input != nil {
		typ := reflect.TypeOf(input)
		if typ.Kind() == reflect.Slice || typ.Kind() == reflect.Array {
			input = reflect.New(typ.Elem()).Interface()
		}
		if v, ok := input.(ManagedObject); ok {
			return true, v.GetObjectMetaData()
		}
	}
	return false, ObjectMetadata{}
}

// PHPIPAMCFG is environment variable that is used to get configuration
const PHPIPAMCFG = "PHPIPAM_CFG"

// PHPIPAM_FILENAME filename to default config file
const PHPIPAM_FILENAME = "phpipam.json"

// HasChangedStrPtr tries to decide if dst string has changed, allowing for nil values.
// src==nil means no value.
// This is to decide if you need to copy src to dst.
func HasChangedStrPtr(src, dst *string, allowBlank bool) bool {
	if src == nil {
		return false
	}
	if dst == nil {
		return true
	}
	if !allowBlank && len(*src) == 0 {
		return false
	}
	return *src != *dst
}
