package phpipam

const VrfPath = "vrf"

// Device represents a device in phpIPAM.
type Vrf struct {
	ID          string  `json:"vrfId"`       // id of VRF
	Name        string  `json:"name"`        // name shown for this VRF
	Description *string `json:"description"` // a description of this VRF
	RD          *string `json:"rd"`          // a RD for this VRF
	Sections    *string `json:"sections"`    // what sections have access to this VRF
}

// CopyFrom copies from src to dst and returns true if anything has changed
func (dst *Vrf) CopyFrom(src Vrf) bool {
	changed := false
	if HasChangedStrPtr(&src.ID, &dst.ID, false) {
		changed = true
		dst.ID = src.ID
	}
	if HasChangedStrPtr(&src.Name, &dst.Name, false) {
		changed = true
		dst.Name = src.Name
	}
	if HasChangedStrPtr(src.Description, dst.Description, true) {
		changed = true
		dst.Description = src.Description
	}
	if HasChangedStrPtr(src.RD, dst.RD, true) {
		changed = true
		dst.RD = src.RD
	}
	if HasChangedStrPtr(src.Sections, dst.Sections, true) {
		changed = true
		dst.Sections = src.Sections
	}
	return changed
}

// GetObjectID returns the object ID for this device
func (d Vrf) GetObjectID() string {
	return d.ID
}

func (d Vrf) GetObjectMetaData() ObjectMetadata {
	return ObjectMetadata{
		BasePath:   VrfPath,
		DeleteOnId: true,
	}
}

// GetAllVrf returns all VRFs that are configured, allowing you to specify your own parameters
func (c *Connection) GetAllVrf(p Param) (result []Vrf, err error) {
	gr, err := c.GetObject(p, Vrf{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}

// FindVrf returns a VRF by its name, nil if it does not exist
func FindVrf(name string, list []Vrf) *Vrf {
	for _, v := range list {
		if v.Name == name {
			return &v
		}
	}
	return nil
}
