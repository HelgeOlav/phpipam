package main

import (
	"bitbucket.org/HelgeOlav/phpipam"
	"encoding/json"
	"fmt"
	"net/http"
)

func getVlans(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.Method, r.URL.Path)
	w.Header().Set("Content-Type", "application/json")
	myDesc := "My description"
	vlans := []phpipam.Vlan{{
		ID:       "1",
		DomainID: "1",
		Name:     "Test VLAN",
		VLAN:     "1",
	},
		{
			ID:          "2",
			DomainID:    "1",
			Name:        "Another VLAN",
			VLAN:        "2",
			Description: &myDesc,
		}}
	result := phpipam.GenericResult{
		Code:    200,
		Success: true,
		Time:    4.345273,
		Data:    vlans,
	}
	// make json
	bytes, err := json.Marshal(result)
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = w.Write(bytes)
	if err != nil {
		fmt.Println(err)
	}
}

func getL2Domains(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	mySec := "S"
	l2dom := []phpipam.L2Domain{{ID: "1", Name: "domain 1", Description: "my desc"}, {ID: "2", Name: "domain 2", Description: "my desc", Sections: &mySec}}
	res := phpipam.GenericResult{
		Code:    200,
		Success: true,
		Time:    0.11,
		Data:    l2dom,
	}
	// make json
	bytes, err := json.Marshal(res)
	if err != nil {
		fmt.Println(err)
		return
	}
	_, err = w.Write(bytes)
	if err != nil {
		fmt.Println(err)
	}
}

func startWeb() {
	http.HandleFunc("/phpipam/api/demo1/l2domains/", getL2Domains)
	http.HandleFunc("/phpipam/api/demo1/vlan/", getVlans)
	http.ListenAndServe(":8080", nil)
}
