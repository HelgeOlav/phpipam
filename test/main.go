package main

import (
	"bitbucket.org/HelgeOlav/phpipam"
	"fmt"
	"time"
)

var conn phpipam.Connection

func main() {
	var err error
	fmt.Println("helge")
	conn, err = phpipam.DefaultConnection()
	if err != nil {
		fmt.Println(err)
		return
	}
	go startWeb()
	res, err := conn.GetL2Domains()
	fmt.Println(err, res)
	conn.DeleteObject(res[1])
	// get vlans
	vlans, err := conn.GetVlans()
	fmt.Println(err, vlans)
	subnets, err := conn.GetSubnetByVlan(vlans[0])
	fmt.Println(err, subnets)
	_, err = conn.GetTagById("3")
	fmt.Println("TagById", err)
	// give some time before exit
	time.Sleep(time.Second * 60)
}
