package phpipam

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"github.com/mitchellh/mapstructure"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

// Connection defines how to connect to the server.
type Connection struct {
	ServerURL   string            `json:"server-url"` // base address to the phpIPAM installation
	Token       string            `json:"token"`      // Token key
	AppID       string            `json:"app-id"`     // app-id for this Token key
	Client      http.Client       `json:"-"`
	InsecureSSL bool              `json:"insecure-ssl"`        // set to disable SSL warnings - only checked in DefaultConnection() and LoadConnection()
	Headers     map[string]string `json:"headers,omitempty"`   // extra headers to send on requests, populated in NewRequest()
	DebugDir    string            `json:"debug-dir,omitempty"` // if set, HTTP requests and responses will be saved to this directory
}

type GenericResult struct {
	Code    int         `json:"code"`              // result code
	Success bool        `json:"success"`           // success flag
	Time    float64     `json:"time"`              // time taken
	Message string      `json:"message,omitempty"` // given sometimes in case of errors
	Data    interface{} `json:"data,omitempty"`    // result from the query, if present
	ID      string      `json:"id"`                // ID of created object
}

// DefaultConnection attempts to create a connection object by either finding the setting in environment variables,
// loading a configured file or loading "phpipam.json" from the current directory as the last resort.
func DefaultConnection() (Connection, error) {
	hostName := os.Getenv("PHPIPAM_URL")
	if len(hostName) > 0 {
		conn := Connection{
			ServerURL:   hostName,
			Token:       os.Getenv("PHPIPAM_API"),
			AppID:       os.Getenv("PHPIPAM_APP"),
			InsecureSSL: os.Getenv("PHPIPAM_INSECURE") == "true",
		}
		if conn.InsecureSSL {
			conn.IgnoreSSLCerts()
		}
		return conn, nil
	}
	fileName := os.Getenv(PHPIPAMCFG)
	if len(fileName) > 0 {
		return LoadConnection(fileName)
	}
	return LoadConnection(PHPIPAM_FILENAME)
}

// LoadConnection creates a connection object from file
func LoadConnection(fn string) (Connection, error) {
	var conn Connection
	myBytes, err := ioutil.ReadFile(fn)
	if err == nil {
		err = json.Unmarshal(myBytes, &conn)
		if err == nil && conn.InsecureSSL {
			conn.IgnoreSSLCerts()
		}
	}
	return conn, err
}

// IgnoreSSLCerts call this helper method to disable SSL certificate warnings
func (c *Connection) IgnoreSSLCerts() {
	if c != nil {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		c.Client.Transport = tr
	}
}

// Param contains data for the specific request like query parameters and context.
// There are many helper methods for setting the parameters that you need.
type Param struct {
	QueryParams   map[string]string // added onto URI
	Headers       map[string]string // added as request headers
	RequestSuffix string            // used internally to append something to the base-path for a given object
	Ctx           context.Context   // context passed to http
}

// NewParam creates a new empty Parameter set, used to construct requests.
func NewParam() Param {
	p := Param{map[string]string{}, map[string]string{}, "", context.Background()}
	return p
}

// DoAndParseRequest will handle any HTTP method and parse a JSON result into result. If the HTTP result code is not 2xx
// then an error is thrown. This function us usually called from the wrapper function, and in turn uses NewRequest to
// build the HTTP request. If you pass a body is will also be marshalled as JSON and sent out.
func (c *Connection) DoAndParseRequest(path, method string, p Param, body interface{}, result interface{}) error {
	// nil check
	if c == nil {
		return errors.New("nil Connection to DoAndParseRequest")
	}
	tmpFile := GetTimestamp()
	// marshal body if present
	var bodyReader io.Reader = nil
	if body != nil {
		bodyBytes, err := json.Marshal(body)
		if err != nil {
			return err
		}
		c.debugSave(tmpFile+"-body.json", body)
		bodyReader = bytes.NewReader(bodyBytes)
	}
	// create request object
	req, err := c.NewRequest(method, path, p, bodyReader)
	if err != nil {
		return err
	}
	// debug log request
	c.debugSave(tmpFile+"-request.json", req.URL)
	// do request
	resp, err := c.Client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// if not 2xx return error
	if resp.StatusCode > 299 || resp.StatusCode < 200 {
		return errors.New(req.URL.Path + " returned " + resp.Status)
	}
	// read response
	resultBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	c.debugSave(tmpFile+"-response.json", string(resultBody))
	// parse the response and return
	err = json.Unmarshal(resultBody, result)
	return err
}

// NewRequest creates generic HTTP requests to phpIPAM using all the parameters specified
// In return you get a Request object that need to complete and close when you are done. The URL is modified
// and the suffix from Param is added here.
// HTTP headers are also added from Conn and Param in this order.
func (c *Connection) NewRequest(method, path string, p Param, body io.Reader) (*http.Request, error) {
	if c == nil {
		return nil, errors.New("nil Connection to NewGetRequest")
	}
	// set our own parameters
	p = p.Token(c.Token)
	// build query values
	query := url.Values{}
	for k, v := range p.QueryParams {
		query.Set(k, v)
	}
	// construct URL
	paramUrl := c.ServerURL + "/api/" + c.AppID + "/" + path + "/"
	if len(p.RequestSuffix) > 0 {
		paramUrl = paramUrl + p.RequestSuffix + "/"
	}
	// create a request
	req, err := http.NewRequestWithContext(p.Ctx, method, paramUrl, body)
	if err == nil {
		req.URL.RawQuery = query.Encode()
		req.Header.Set("Content-Type", "application/json")
		for k, v := range c.Headers {
			req.Header.Set(k, v)
		}
		for k, v := range p.Headers {
			req.Header.Set(k, v)
		}
	}
	return req, err
}

// Token add access token to parameter string. Any existing token is replaced by this one.
func (p Param) Token(token string) Param {
	p.Headers["token"] = token
	return p
}

// Generic set generic parameter string.
func (p Param) Generic(k, v string) Param {
	p.QueryParams[k] = v
	return p
}

// Context set context used for the HTTP call.
func (p Param) Context(c context.Context) Param {
	p.Ctx = c
	return p
}

// DefaultValue sets a key/value if it is not present already
func (p Param) DefaultValue(k, v string) Param {
	if _, ok := p.QueryParams[k]; !ok {
		p.QueryParams[k] = v
	}
	return p
}

// Interface2Object helper method to convert anything to an object as long as it can be parsed.
// Used to convert map[string]interface{} from generic commands
func Interface2Object(input interface{}, result interface{}) error {
	config := &mapstructure.DecoderConfig{TagName: "json", Result: result}
	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return err
	}
	return decoder.Decode(input)
}

// DeleteObject deletes the object from the database
func (c *Connection) DeleteObject(o interface{}) error {
	if c == nil {
		return errors.New("DeleteObject: nil Connection passed")
	}
	ok, obj := GetObjectMetadata(o)
	if !ok {
		return errors.New("DeleteObject: input not ManagedObject")
	}
	if obj.ReadOnly {
		return errors.New("Object is read-only")
	}
	p := NewParam()
	if obj.DeleteOnId {
		p.RequestSuffix = o.(ManagedObject).GetObjectID()
	}
	var result GenericResult
	err := c.DoAndParseRequest(obj.BasePath, http.MethodDelete, p, nil, &result)
	if err == nil {
		if result.Success == false {
			err = errors.New("DeleteObject: delete failed, code " + strconv.Itoa(result.Code))
		}
	}
	return err
}

// CreateObject creates new objects.
func (c *Connection) CreateObject(p Param, object interface{}) (GenericResult, error) {
	var result GenericResult
	var err error
	isReadable, md := GetObjectMetadata(object)
	if isReadable {
		if md.ReadOnly {
			return result, errors.New("Object is read-only")
		}
		myUrl := md.BasePath
		err = c.DoAndParseRequest(myUrl, http.MethodPost, p, object, &result)
		if err == nil {
			if result.Code <= 200 && result.Code > 300 {
				err = errors.New(strconv.Itoa(result.Code) + " " + result.Message)
			}
		}
	} else {
		return result, errors.New("CreateObject: not ManagedObject passed as input")
	}
	return result, err
}

// GetObject is a generic way to retrieve data from phpIPAM. In object you need to pass
// a struct that is the result from the GET operation that fits into Result. object can be either an array or
// just a struct depending on what data you expect back. object has to implement ManagedObject for this call
// to succeed.
// Note that even when you pass object in, the result in GenericResult.Data will be of type map[string]interface{}
func (c *Connection) GetObject(p Param, object interface{}) (GenericResult, error) {
	var result GenericResult
	var err error
	isReadable, md := GetObjectMetadata(object)
	if isReadable {
		myUrl := md.BasePath
		err = c.DoAndParseRequest(myUrl, http.MethodGet, p, nil, &result)
		if err == nil {
			if result.Code != http.StatusOK {
				err = errors.New(strconv.Itoa(result.Code) + " " + result.Message)
			}
		}
	} else {
		err = errors.New("GetObject: Not ManagedObject passed as input")
	}
	return result, err
}

// GetTimestamp returns a string that is the current time in nanoseconds
func GetTimestamp() string {
	time := time.Now().UnixNano()
	return strconv.FormatInt(time, 10)
}

// TODO: add support for ObjectMetadata.PatchOnId
// UpdateObject update an existing object
func (c *Connection) UpdateObject(p Param, object interface{}) (GenericResult, error) {
	var result GenericResult
	var err error
	isReadable, md := GetObjectMetadata(object)
	if isReadable {
		if md.ReadOnly {
			return result, errors.New("Object is read-only")
		}
		if md.PatchOnId {
			p.RequestSuffix = object.(ManagedObject).GetObjectID()
		}
		myUrl := md.BasePath
		err = c.DoAndParseRequest(myUrl, http.MethodPatch, p, object, &result)
		if err == nil {
			if result.Code <= 200 && result.Code > 300 {
				err = errors.New(strconv.Itoa(result.Code) + " " + result.Message)
			}
		}
	} else {
		return result, errors.New("CreateObject: not ManagedObject passed as input")
	}
	return result, err
}

func (c *Connection) debugSave(fn string, object interface{}) {
	if c != nil && len(c.DebugDir) > 0 && len(fn) > 0 {
		outFile := filepath.Join(c.DebugDir, fn)
		outBytes, err := json.Marshal(object)
		if err == nil {
			f, err := os.Create(outFile)
			if err == nil {
				_, _ = f.Write(outBytes)
				f.Close()
			}
		}
	}
}
