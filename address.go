package phpipam

const AddressPath = "addresses"

// Address is an address in phpIPAM
type Address struct {
	Id          string  `json:"id"`
	SubnetId    string  `json:"subnetId"`
	IP          string  `json:"ip"`
	Description *string `json:"description,omitempty"`
	Hostname    string  `json:"hostname,omitempty"`
	Tag         string  `json:"tag"`
	DeviceID    *string `json:"deviceId,omitempty"`
	Owner       *string `json:"owner,omitempty"`
}

func (a Address) GetObjectMetaData() ObjectMetadata {
	return ObjectMetadata{BasePath: AddressPath}
}

func (a Address) GetObjectID() string {
	return a.Id
}

// GetAllAddresses returns all addresses in phpIPAM.
func (c *Connection) GetAllAddresses() (result []Address, err error) {
	gr, err := c.GetObject(NewParam(), Address{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}
