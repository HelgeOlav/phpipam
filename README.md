# phpIPAM implementation for Go

This is a framework to work with phpIPAM. It is still in early progress. The goal is to make
a framework that can be used to work easily with phpIPAM using token and app authentication.

Most of the magic is in conn.go and defs.go where the interface ManagedObject is used to work on all
objects that have the same pattern and syntax for GET/PUT/POST etc.

When you want to implement a new controller that uses this syntax, only a few lines of code is neeed to
work with the object.

phpIPAM is mainly created for its web GUI. The API is weak and does not support all features in the GUI.
The API shows the internal data model, and often you might end up with several queries to get the data you want.

## Getting started

The easiest way to get started is to load a default connection object.

```go
conn, err = phpipam.DefaultConnection()
```

The connection object returned (if it does not fail) is a result of either:

1. Environment variables (PHPIPAM_URL, PHPIPAM_API, PHPIPAM_APP and PHPIPAM_INSECURE).
2. Config file from environment PHPIPAM_CFG.
3. The config file phpipam.json in the current directory.

Look in the test folder for a sample configuration file.

With the connection object you now can do lots of stuff. To get a a list of all L2 domains use:


```go
domains, err := conn.GetL2Domains()
```