package phpipam

import "testing"

func TestHasChangedStrPtr(t *testing.T) {
	// test all four combinations of nil/non-nil
	str := "test"
	str2 := "fake"
	if HasChangedStrPtr(nil, nil, false) {
		t.Error("Should be false on two nil")
	}
	if HasChangedStrPtr(nil, &str, false) {
		t.Error("Should be false when src==nil and dst!=nil")
	}
	if !HasChangedStrPtr(&str, nil, false) {
		t.Error("Should be true when dst=nil")
	}
	if HasChangedStrPtr(&str, &str, false) {
		t.Error("Should be false when strings are equal")
	}
	if !HasChangedStrPtr(&str, &str2, false) {
		t.Error("Should be true when strings are unequal")
	}
	str = ""
	if HasChangedStrPtr(&str, &str2, false) {
		t.Error("blank src should not copy")
	}
	if !HasChangedStrPtr(&str, &str2, true) {
		t.Error("blank src should copy")
	}
}
