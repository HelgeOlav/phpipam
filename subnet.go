package phpipam

const SubnetPath = "subnets"

// Subnet represents a subnet in phpIPAM
type Subnet struct {
	ID           string `json:"id,omitempty"`
	Subnet       string `json:"subnet"`
	Mask         string `json:"mask"`
	SectionID    string `json:"sectionId,omitempty"`
	Description  string `json:"description,omitempty"`
	VrfID        string `json:"vrfId,omitempty"`
	MasterSubnet string `json:"masterSubnetId,omitempty"`
	VlanID       string `json:"vlanId,omitempty"`
	Device       string `json:"device,omitempty"`
	Tag          string `json:"tag,omitempty"`
	NameServerId string `json:"nameserverId,omitempty"`
	GatewayID    string `json:"gatewayId,omitempty"`
}

func (s Subnet) GetObjectMetaData() ObjectMetadata {
	return ObjectMetadata{BasePath: SubnetPath}
}

func (s Subnet) GetObjectID() string {
	return s.ID
}
