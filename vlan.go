package phpipam

const VlanPath = "vlan"

// Vlan represents a VLAN in phpIPAM.
type Vlan struct {
	ID          string  `json:"vlanId"`                // id in database (not related to the VLAN ID on the switch!!!)
	DomainID    string  `json:"domainId"`              // l2domain
	Name        string  `json:"name"`                  // name of VLAN
	VLAN        string  `json:"number"`                // VLAN ID
	Description *string `json:"description,omitempty"` // optional description
	EditDate    *string `json:"editDate,omitempty"`
}

func (v Vlan) GetObjectMetaData() ObjectMetadata {
	return ObjectMetadata{BasePath: VlanPath, DeleteOnId: true}
}

// CopyFrom copy non-empty values from src, is true if anything changed
func (dst *Vlan) CopyFrom(src Vlan) bool {
	changed := false
	if dst != nil {
		if HasChangedStrPtr(&src.ID, &dst.ID, false) {
			dst.ID = src.ID
			changed = true
		}
		if HasChangedStrPtr(&src.DomainID, &dst.DomainID, false) {
			dst.DomainID = src.DomainID
			changed = true
		}
		if HasChangedStrPtr(&src.Name, &dst.Name, false) {
			dst.Name = src.Name
			changed = true
		}
		if HasChangedStrPtr(&src.VLAN, &dst.VLAN, false) {
			dst.VLAN = src.VLAN
			changed = true
		}
		if HasChangedStrPtr(src.Description, dst.Description, true) {
			dst.Description = src.Description
			changed = true
		}
		if HasChangedStrPtr(src.EditDate, dst.EditDate, true) {
			dst.EditDate = src.EditDate
			changed = true
		}
	}
	return changed
}

func (v Vlan) GetObjectID() string {
	return v.ID
}

// GetVlans returns all VLANs that are configured in a domain
func (c *Connection) GetVlansFromDomain(domain string) (result []Vlan, err error) {
	p := NewParam()
	p.RequestSuffix = domain + "/vlans"
	gr, err := c.GetObject(p, []L2Domain{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}

// GetVlans returns all VLANs that are configured
func (c *Connection) GetVlans() (result []Vlan, err error) {
	gr, err := c.GetObject(NewParam(), Vlan{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}

// FindVlan Looks through the list of VLANS, find the one that matches ID and domain
func FindVlan(VLAN, domain string, vlans []Vlan) *Vlan {
	for _, v := range vlans {
		if v.VLAN == VLAN && v.DomainID == domain {
			return &v
		}
	}
	return nil
}

// GetSubnetByVlan Returns a list of subnets that is assigned to the specific VLAN.
func (c *Connection) GetSubnetByVlan(v Vlan) (result []Subnet, err error) {
	p := NewParam()
	p.RequestSuffix = v.ID + "/subnets"
	gr, err := c.GetObject(p, Vlan{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}
