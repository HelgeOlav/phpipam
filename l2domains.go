package phpipam

const L2DomainsPath = "l2domains"

type L2Domain struct {
	ID          string  `json:"id,omitempty"`
	Name        string  `json:"name"`
	Description *string `json:"description,omitempty"`
	Sections    *string `json:"sections,omitempty"`
}

// CopyFrom copies from src object, leaving empty attributes. Return true if anything changed.
func (dst *L2Domain) CopyFrom(src L2Domain) bool {
	changed := false
	if dst != nil {
		if HasChangedStrPtr(&src.ID, &dst.ID, false) {
			dst.ID = src.ID
			changed = true
		}
		if HasChangedStrPtr(&src.Name, &dst.Name, false) {
			dst.Name = src.Name
			changed = true
		}
		if HasChangedStrPtr(src.Description, dst.Description, true) {
			dst.Description = src.Description
			changed = true
		}
		if HasChangedStrPtr(src.Sections, dst.Sections, true) {
			dst.Sections = src.Sections
			changed = true
		}
	}
	return changed
}

func (l L2Domain) GetObjectID() string {
	return l.ID
}

func (l L2Domain) GetObjectMetaData() ObjectMetadata {
	return ObjectMetadata{
		BasePath:   L2DomainsPath,
		DeleteOnId: true,
	}
}

// GetL2Domains returns all L2Domains that are configured
func (c *Connection) GetL2Domains() (result []L2Domain, err error) {
	gr, err := c.GetObject(NewParam(), L2Domain{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}

// FindL2Domain looks through the list and returns the L2Domain with the specified name, nil if not found
func FindL2Domain(name string, domains []L2Domain) *L2Domain {
	for _, v := range domains {
		if v.Name == name {
			return &v
		}
	}
	return nil
}
