module bitbucket.org/HelgeOlav/phpipam

go 1.17

require (
	bitbucket.org/HelgeOlav/ansiblemodule v0.0.0-20210713093639-1de85cb97058
	bitbucket.org/HelgeOlav/utils v0.0.0-20211209011010-71743d09240c
	github.com/mitchellh/mapstructure v1.4.2
)
