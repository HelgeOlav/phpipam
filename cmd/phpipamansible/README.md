# phpIPAM Ansible module

phpipamansible has lots of modules for using phpIPAM and Ansible together.

## Installation

To install these modules compile this folder using ```go build .``` and run the program with "--link".
That will give you some commands you can run to create symlinks in the Ansible modules directory.

The program looks at what name it was launched by to invoke the right module.

## Configuration

The only thing you need to do is to tell the module how to connect to phpIPAM. This can be done in one of several ways:

1. A config file called phpipam.json in the current directory.
2. The same config file anywhere, located by the environment variable PHPIPAM_CFG.
3. The same config file anyhwere, specified in the Ansible playbook as parameter PHPIPAM_CFG.
4. You can use the environment variables PHPIPAM_URL, PHPIPAM_API, PHPIPAM_APP and PHPIPAM_INSECURE to specify the settings.

When you think you have it right you can invoke ```ansible all -m phpipam.ping``` and see if you get an error message.
This command will not connect to the database but will fail if it can't get the configuration.

For all network related modules you must ensure that Ansible will work on your computer, not on the network device.
You can use ```delegate_to: localhost``` on a task to make sure it will be run locally. Read the Ansible documentation, and the part
about network management.

# Modules

Most of the modules are idempotent, with a few exceptions that are mentioned in the relevant modules.

For most modules these parameters are common and behave the same way.

* state: absent|present
* lookup: true|false
* PHPIPAM_CFG: <path_to_file>

For most modules you have to specify ```state: absent``` or ```state: present```.
The module will respond with changed only if there were a change, and it will fail if you don't specify your intent.

```lookup: true``` can be used to set the module in "read-only" mode. It will get the data you want, but won't change anything
even if your Ansible playbook tries to change something. It will fail if it has to create an object. It will also fail if you use it with ```state: absent```.

## phpipam.ping

This module is only used so you can test that the modules have been installed correctly and works.

No parameters are needed, except for PHPIPAM_CFG if you want to use that.

## phpipam.l2domain

This module will create/edit/delete/get L2 domains in phpIPAM. You will need this to work with VLAN's.

```yaml
- name: Get L2 domain
  phpipam.l2domain:
    name: 'My home domain'
    state: present
    description: 'This is where I can write something about this domain'
    sections: '1;2'
  register: myl2domain
  delegate_to: localhost
```

In this module, only name and state are required parameters. This module is idempotent and you can safely call the same
module many times. The first time it will create the L2-domain if it does not exist.

The return value from this module is the domain-ID that you will use later when working with VLAN's.

## phpipam.vlan

This module will create/edit/delete/get L2 domains in phpIPAM. To use VLAN's you first need to get the L2-domain ID as shown above.

```yaml
- name: Get VLAN
  phpipam.vlan:
    name: 'SRV_ENT_NETWORK'
    VLAN: "100"
    domainId: '{{ myl2domain }}'
    state: present
    description: 'This is where I can write something about this VLAN'
    sections: '1;2'
  register: myvlan
  delegate_to: localhost
```

In this module the required parameters are state, domainId, VLAN and name. You are allowed to have overlapping VLAN ID's
and therefore name, VLAN and domainId together is what makes this VLAN unique. (You cannot change the name after it has been created.)

This module is idempotent, and you can safely call the same
module many times. The first time it will create the VLAN if it does not exist.

The ID is returned and you need that for other operations. It is not the ID for the VLAN but an phpIPAM internal reference.

## phpipam.nextvlan

This module is not idempotent. It will find the first available VLAN ID in a L2 domain. This module does not update the database,
only returns the first available so it can be used with phpipam.vlan to create a new account.

```yaml
- name: Find free VLAN
  phpipam.nextvlan:
    domainId: '{{ myl2domain }}'
    from: 1000
  register: anUnnasignedVlan
  delegate_to: localhost
```

It will return the first available VLAN starting from VLAN 1000 and going upwards. If there are no free VLAN's then this
module will fail. If from is not specified it will search from VLAN 2 and upwards.

## phpipam.getvlans

This module will return all vlans inside an l2domain. It will return an array of vlan_id and name of everything inside the l2domain.
You get the domainId by using phpipam.l2domain (see above).

```yaml
- name: Get all VLANs
  phpipam.getvlans:
    domainId: '{{ myl2domain }}'
  register: allVlans
  delegate_to: localhost
- name: Show Vlans
  debug:
    msg: '{{ allVlans }}'
  delegate_to: localhost
```

## phpipam.getdevices

This module will return all devices in the database, in phpIPAM internal format for you to work on.

```yaml
- name: Get list of all devices
  phpipam.getdevices:
  register: allDevices
  delegate_to: localhost
- name: Show devices
  debug:
    msg: '{{ allDevices }}'
  delegate_to: localhost
```

This module only returns all data in the database and can safely be called many times.

## phpipam.device

This module will create/edit/delete/get devices in phpIPAM.

```yaml
- name: Idempotent operation on my device
  phpipam.device:
    name: '{{ inventory_hostname }}'
    state: present
    type: '4'
    ip: '1.2.3.4'
    description: 'My great device'
  register: mydevice
  delegate_to: localhost
```

The only mandatory parameters are state, name and type(if state: present). If a device is not there it will be created,
otherwise returned. ```lookup: true``` can be used if you want a read-only operation.

The result is a device ID that you can use to link this device to other objects (subnet/IP address).