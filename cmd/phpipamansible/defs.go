package main

import "errors"

// ansibleParams common variables from Ansible
type ansibleParams struct {
	State     *string // [ present, absent ]
	Lookup    bool    // if true the intent is only to look up data, not update
	isPresent bool    // set by validation
}

func (a *ansibleParams) Validate() error {
	if a == nil {
		return errors.New("nil passed to Validate()")
	}
	if a.State == nil {
		return errors.New("missing state")
	} else {
		switch *a.State {
		case StatePresent:
			a.isPresent = true
		case StateAbsent:
			a.isPresent = false
		default:
			return errors.New("invalid state, got " + *a.State)
		}
	}
	if a.Lookup && !a.isPresent {
		return errors.New("cannot do lookup:true and state:absent")
	}
	return nil
}

const (
	StatePresent = "present"
	StateAbsent  = "absent"
)
