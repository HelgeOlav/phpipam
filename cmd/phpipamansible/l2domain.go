package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
	"github.com/mitchellh/mapstructure"
)

func init() {
	allModules = append(allModules, ansibleModule{
		module: "phpipam.l2domain",
		exec:   l2DomainFunc,
		desc:   "Work on a l2domains",
	})
}

func l2DomainFunc() {
	req, conn := moduleInit()
	var err error
	// get request parameters
	var reqParams ansibleParams
	var reqData phpipam.L2Domain
	_ = mapstructure.Decode(req, &reqParams)
	_ = mapstructure.Decode(req, &reqData)
	// validate input
	err = reqParams.Validate()
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	if len(reqData.Name) == 0 {
		ansiblemodule.FailResponse("missing name")
	}
	// get all domains and find the one we are working on
	domains, err := conn.GetL2Domains()
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	myDomain := phpipam.FindL2Domain(reqData.Name, domains)
	// add or delete
	if reqParams.isPresent {
		if reqParams.Lookup {
			if myDomain == nil {
				ansiblemodule.FailResponse("lookup: no such L2Domain")
			} else {
				ansiblemodule.SuccessResponse(false, myDomain.ID)
			}
		} // if Lookup
		if myDomain == nil { // create new object
			res, err := conn.CreateObject(phpipam.NewParam(), reqData)
			if err != nil {
				ansiblemodule.FailResponse(err.Error())
			} else {
				ansiblemodule.SuccessResponse(true, res.ID)
			}
		} else { // myDomain != nil - update
			if myDomain.CopyFrom(reqData) {
				_, err := conn.UpdateObject(phpipam.NewParam(), *myDomain)
				if err != nil {
					ansiblemodule.FailResponse(err.Error())
				} else {
					ansiblemodule.SuccessResponse(true, myDomain.ID)
				}
			} else {
				ansiblemodule.SuccessResponse(false, myDomain.ID)
			}
		} // myDomain == nil
	} else { // !state: present
		if myDomain == nil {
			ansiblemodule.SuccessResponse(false, "not in phpIPAM")
		} else {
			err = conn.DeleteObject(*myDomain)
			if err == nil {
				ansiblemodule.SuccessResponse(true, "deleted")
			} else {
				ansiblemodule.FailResponse(err.Error())
			}
		}
	} // state: present
}
