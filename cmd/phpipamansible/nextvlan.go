package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
	"fmt"
	"strconv"
)

func init() {
	allModules = append(allModules, ansibleModule{
		module: "phpipam.nextvlan",
		exec:   nextVlanFunc,
		desc:   "Return the next free VLAN ID in a domain",
	})
}

func nextVlanFunc() {
	req, conn := moduleInit()
	// check input
	for _, v := range []string{"domainId"} {
		if len(req.GetKeyAsString(v)) == 0 {
			ansiblemodule.FailResponse("Missing key " + v)
		}
	}
	// download VLAN list
	domainId := req.GetKeyAsString("domainId")
	domains, err := conn.GetVlansFromDomain(domainId)
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	// VLAN ID to search from
	StartId := req.GetKeyAsInt("from")
	if StartId < 1 {
		StartId = 2
	}
	fmt.Println(domains)
	for StartId < 4096 {
		vlanAsString := strconv.Itoa(StartId)
		if phpipam.FindVlan(vlanAsString, domainId, domains) == nil {
			ansiblemodule.SuccessResponse(false, vlanAsString)
		}
		StartId++
	}
	ansiblemodule.FailResponse("no available VLAN")
}
