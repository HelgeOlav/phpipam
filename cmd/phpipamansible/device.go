package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
	"github.com/mitchellh/mapstructure"
)

func init() {
	allModules = append(allModules, ansibleModule{
		module: "phpipam.device",
		exec:   deviceFunc,
		desc:   "Work on a device object",
	})
}

func deviceFunc() {
	req, conn := moduleInit()
	var err error
	// get request parameters
	var reqParams ansibleParams
	var reqData phpipam.Device
	_ = mapstructure.Decode(req, &reqParams)
	_ = mapstructure.Decode(req, &reqData)
	// validate input
	err = reqParams.Validate()
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	if len(reqData.Name) == 0 {
		ansiblemodule.FailResponse("missing name")
	}
	// process data,
	// if present: load all devices, find the one we want, merge changes and save back
	// if absent: load all devices, find the one we want and delete it
	devices, err := conn.GetDevicesByKeyword(reqData.Name)
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	curDevice := phpipam.FindDevice(reqData.Name, &devices)

	if reqParams.isPresent {
		// if lookup - handle that first (read-only)
		if reqParams.Lookup {
			if curDevice != nil && len(curDevice.ID) > 0 {
				ansiblemodule.SuccessResponse(false, curDevice.ID)
			} else {
				ansiblemodule.FailResponse("lookup: no such device")
			}
		}
		// continue with update
		if curDevice != nil {
			if !curDevice.CopyFrom(reqData) {
				ansiblemodule.SuccessResponse(false, curDevice.ID)
			}
			gr, err := conn.UpdateObject(phpipam.NewParam(), *curDevice)
			if err != nil {
				ansiblemodule.FailResponse(err.Error())
			}
			if gr.Success {
				ansiblemodule.SuccessResponse(true, curDevice.ID)
			}
		} else { // curDevice == nil
			gr, err := conn.CreateObject(phpipam.NewParam(), reqData)
			if err == nil {
				if !gr.Success {
					ansiblemodule.FailResponse(gr.Message)
				}
				ansiblemodule.SuccessResponse(true, gr.ID)
			} else {
				ansiblemodule.FailResponse(err.Error())
			}
		}
	} else {
		if curDevice == nil {
			ansiblemodule.SuccessResponse(false, "device not found")
		} else {
			err = conn.DeleteObject(*curDevice)
			if err == nil {
				ansiblemodule.SuccessResponse(true, "object deleted")
			} else {
				ansiblemodule.FailResponse(err.Error())
			}
		}
	}
}
