package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
	"bitbucket.org/HelgeOlav/utils"
	"github.com/mitchellh/mapstructure"
	"strconv"
)

func init() {
	allModules = append(allModules, ansibleModule{
		module: "phpipam.getvlans",
		exec:   vlansFunc,
		desc:   "Get list of VLANs",
	})
}

// vlansFunc returns a list of VLANs inside a l2provider
func vlansFunc() {
	req, conn := moduleInit()
	var err error
	// get request parameters
	var reqData phpipam.Vlan
	_ = mapstructure.Decode(req, &reqData)
	// validate input
	if len(reqData.DomainID) == 0 {
		ansiblemodule.FailResponse("missing domain")
	}
	// get all domains and find the one we are working on
	vlans, err := conn.GetVlansFromDomain(reqData.DomainID)
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	// convert list and return it
	type MyVlan struct {
		ID   int    `yaml:"vlan_id" json:"vlan_id"` // ID of VLAN inside provider (not ID in database!)
		Name string `json:"name" yaml:"name"`       // name of VLAN
	}
	var resultVlans []MyVlan
	for _, v := range vlans {
		newVlan := MyVlan{Name: utils.OnlyAsciiText(v.Name)}
		newVlan.ID, err = strconv.Atoi(v.VLAN)
		if err != nil {
			ansiblemodule.FailResponse(err.Error())
		}
		if newVlan.ID < 1 || newVlan.ID > 4095 {
			ansiblemodule.FailResponse("Invalid VLAN " + newVlan.Name + "(" + v.VLAN + ")")
		}
		resultVlans = append(resultVlans, newVlan)
	}
	ansiblemodule.SuccessResponse(false, resultVlans)
}
