package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
)

func init() {
	allModules = append(allModules, ansibleModule{
		module: "phpipam.getdevices",
		exec:   getDevices,
		desc:   "Get a list of all devices",
	})
}

func getDevices() {
	_, conn := moduleInit()
	// get devices
	result, err := conn.GetDevices()
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	// and produce output
	ansiblemodule.SuccessResponse(false, result)
}
