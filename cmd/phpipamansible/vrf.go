package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
	"github.com/mitchellh/mapstructure"
)

func init() {
	allModules = append(allModules, ansibleModule{
		module: "phpipam.vrf",
		exec:   vrfFunc,
		desc:   "Work on VRF",
	})
}

func vrfFunc() {
	req, conn := moduleInit()
	var err error
	// get request parameters
	var reqParams ansibleParams
	var reqData phpipam.Vrf
	_ = mapstructure.Decode(req, &reqParams)
	_ = mapstructure.Decode(req, &reqData)
	// validate input
	err = reqParams.Validate()
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	if len(reqData.Name) == 0 {
		ansiblemodule.FailResponse("missing name")
	}
	// get all VRFs from the database (no search capability in API)
	vrfs, err := conn.GetAllVrf(phpipam.NewParam())
	if err != nil {
		ansiblemodule.FailResponse(err.Error())
	}
	myVrf := phpipam.FindVrf(reqData.Name, vrfs)
	// now we can produce something
	if reqParams.isPresent {
		changed, result, err := vrfWorkPresent(&reqData, myVrf, &conn)
		if err != nil {
			ansiblemodule.FailResponse(err.Error())
		} else {
			ansiblemodule.SuccessResponse(changed, result.ID)
		}
	} else { // if isPresent

	} // if isPresent
}

// vrfWorkPresent will handle all the work when state=present
func vrfWorkPresent(fromAnsible, fromPhpipam *phpipam.Vrf, conn *phpipam.Connection) (changed bool, result *phpipam.Vrf, err error) {
	// handle creation of new object
	if fromPhpipam == nil {
		gr, err := conn.CreateObject(phpipam.NewParam(), fromAnsible)
		if err != nil {
			return false, nil, err
		}
		fromAnsible.ID = gr.ID
		return true, fromAnsible, nil
	}
	// this is an update, handle it
	changed = fromPhpipam.CopyFrom(*fromAnsible)
	if changed {
		_, err = conn.UpdateObject(phpipam.NewParam(), fromPhpipam)
	}
	// and return
	result = fromPhpipam
	return
}
