package main

import (
	"bitbucket.org/HelgeOlav/phpipam"
	"bitbucket.org/HelgeOlav/utils/version"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func printUsage(extra *string) {
	ver := version.Get()
	fmt.Println(ver.String())
	fmt.Println(os.Args[0]) // for debugging purposes
	fmt.Println("This executable is an Ansible module for phpIPAM. It is invoked by Ansible and should be called on its symlinks.\n\nModules:")
	for _, v := range allModules {
		fmt.Printf("%20v - %v\n", v.module, v.desc)
	}
	if extra != nil {
		fmt.Println(*extra)
	}
	os.Exit(1)
}

const ansibleExePrefix = "AnsiballZ_"

func main() {
	_, filename := filepath.Split(os.Args[0])
	allModules = append(allModules, ansibleModule{
		module:    filename,
		exec:      mainHelper,
		noSymlink: true,
		desc:      "When not called from Ansible",
	})
	// during development, you can call a module my specifying its name as an environment variable
	modPath := os.Getenv("PHPIPAMANSIBLE")
	if len(modPath) > 0 {
		filename = modPath
	}
	if strings.HasPrefix(filename, ansibleExePrefix) {
		filename = filename[len(ansibleExePrefix):]
	}
	// run the appropriate module
	for _, v := range allModules {
		if v.module == filename {
			v.exec() // this call should never return
			os.Exit(1)
		}
	}
	printUsage(nil)
}

func mainHelper() {
	desc := "\nCall with\n\t--link <path> to get all symlinks\n\t--remove <path> to remove symlinks\n\t--help for help"
	if len(os.Args) < 2 {
		printUsage(&desc)
	}
	// path to symlinks
	whereto := "~/.ansible/plugins/modules/"
	if len(os.Args) > 2 {
		whereto = os.Args[2]
	}
	// check command line
	switch os.Args[1] {
	case "--remove":
		for _, v := range allModules {
			if !v.noSymlink {
				fmt.Printf("rm -f %v%v\n", whereto, v.module)
			}
		}
		os.Exit(1)
	case "--link":
		src := os.Args[0]
		if src[0] != '/' {
			path, _ := os.Getwd()
			src = path + "/" + src[2:]
		}
		fmt.Println("mkdir -p", whereto)
		for _, v := range allModules {
			if !v.noSymlink {
				fmt.Printf("ln -s %v %v%v\n", src, whereto, v.module)
			}
		}
		os.Exit(1)
	}
	printUsage(&desc)
}

func init() {
	version.NAME = "phpipamansible"
	version.VERSION = phpipam.Version
}
