package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
)

func init() {
	allModules = append(allModules, ansibleModule{
		module: "phpipam.ping",
		exec:   pingFunc,
		desc:   "ping to test that module is working",
	})
}

type pingResult struct {
	Input      ansiblemodule.AnsibleRequest `json:"input_parameters"`
	Ping       string                       `json:"ping"`
	ParsedFail string                       `json:"parsed-fail"`
	AnsModVer  string                       `json:"ansible-lib-ver"`
	PhpIpamVer string                       `json:"phpipam-lib-ver"`
}

func pingFunc() {
	req, _ := moduleInit()
	result := pingResult{
		Input:      req,
		Ping:       "pong",
		ParsedFail: req.GetKeyAsString("fail"),
		AnsModVer:  ansiblemodule.Version,
		PhpIpamVer: phpipam.Version,
	}
	if len(result.ParsedFail) > 0 {
		ansiblemodule.FailResponse(result)
	}
	ansiblemodule.SuccessResponse(false, result)
}
