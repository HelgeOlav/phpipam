package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
	"fmt"
	"os"
)

type moduleExec func()

type ansibleModule struct {
	module    string     // name of called executable
	exec      moduleExec // handler for this module
	desc      string     // command line description of this module
	noSymlink bool       // true if it should not be included in symlinks
}

// allModules is a list of all modules available
var allModules []ansibleModule

// moduleInit helper that returns a request object and a connection object, and fails with os.Exit(1) if any error
func moduleInit() (ansiblemodule.AnsibleRequest, phpipam.Connection) {
	if len(os.Args) < 2 {
		msg := "\nI was called as an Ansible module but from outside Ansible"
		printUsage(&msg)
	}
	// parse Ansible object
	req, err := ansiblemodule.NewAnsibleRequestFromFile(os.Args[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// see if Ansible wants a specific connection profile
	custProf := req.GetKeyAsString("PHPIPAM_CFG")
	// get phpipam connection
	var conn phpipam.Connection
	if len(custProf) == 0 {
		conn, err = phpipam.DefaultConnection()
	} else {
		conn, err = phpipam.LoadConnection(custProf)
		delete(req, phpipam.PHPIPAMCFG)
	}
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return req, conn
}
