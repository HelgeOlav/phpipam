package main

import (
	"bitbucket.org/HelgeOlav/ansiblemodule"
	"bitbucket.org/HelgeOlav/phpipam"
	"bitbucket.org/HelgeOlav/utils/version"
	"encoding/json"
	"fmt"
	"os"
)

type Config struct {
	Categories   ansiblemodule.AnsibleRequest `json:"categories"`              // mapping of categories, numbers as string to string - no spaces in the mapped name!
	Headers      map[string]string            `json:"headers"`                 // headers to use on query
	ConnFile     *string                      `json:"phpipam_connection"`      // path to connection file to load - if empty then DefaultConnection() will be used
	Groups       map[string][]string          `json:"nested_groups,omitempty"` // nesting of groups (children)
	UseIPForConn bool                         `json:"use_ip"`                  // if true IP address on device is used in var ansible_hostname (when defined), default is only to use hostname (from DNS)
}

func LoadConfig(fn string) (c Config, err error) {
	bytes, err := os.ReadFile(fn)
	if err == nil {
		err = json.Unmarshal(bytes, &c)
	}
	return
}

func main() {
	// try to get configuration
	confFile := os.Getenv("PHPIPAMINVENTORY")
	if len(confFile) == 0 {
		confFile = "phpipaminventory.json"
	}
	config, _ := LoadConfig(confFile)
	// Get a connection object
	var conn phpipam.Connection
	var err error
	if config.ConnFile == nil {
		conn, err = phpipam.DefaultConnection()
	} else {
		conn, err = phpipam.LoadConnection(*config.ConnFile)
	}
	if err != nil {
		ver := version.Get()
		fmt.Println(ver.String())
		fmt.Println(err)
		os.Exit(1)
	}

	// prepare for query
	p := phpipam.NewParam()
	for k, v := range config.Headers {
		p.Headers[k] = v
	}
	// now get a list of devices
	devices, err := conn.GetDevicesWithParam(p)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// convert device list to output format
	output := ansiblemodule.AnsibleInventory{}
	output.Groups = config.Groups
	for _, v := range devices {
		host := ansiblemodule.AnsibleHost{
			Host: v.Name,
		}
		// if an IP is defined then add that as an ansible_connection variable (otherwise the hostname is used)
		host.Vars = make(map[string]string)
		if v.IP != nil && len(*v.IP) > 0 && config.UseIPForConn {
			host.Vars["ansible_host"] = *v.IP
		}
		// convert device type into group
		devType := config.Categories.GetKeyAsString(v.Type)
		if len(devType) == 0 {
			devType = "category_" + v.Type
		}
		host.Groups = []string{devType}
		output.Hosts = append(output.Hosts, host)
	}

	// and serve the output
	output.Serve()
}

func init() {
	version.NAME = "phpipaminventory"
	version.VERSION = phpipam.Version
}
