# phpipaminventory

This program is a dynamic inventory in Ansible that returns devices from phpIPAM.
All devices will be mapped to a device group matching its category.

Configuration for this program is found in either:

1. Environment variable PHPIPAMINVENTORY that points to a file to load.
2. The file "phpipaminventory.json" loaded from the current directory.

This file contain mappings between category ID and category name. This is not supported in the API and will have to be configured manually.
In case the a category name is not found the Ansible group will just be "category_<number>".

Configuration to connect to phpIPAM is done using the [phpipam](https://bitbucket.org/HelgeOlav/phpipam) framework.
This can be overridden in this programs configuration file by creating a property "phpipam_connection" that points to your connection profile.

Ansible integration is done using [ansiblemodule](https://bitbucket.org/HelgeOlav/ansiblemodule) that supports inventories.