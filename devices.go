package phpipam

const DevicesPath = "devices"

// Device represents a device in phpIPAM.
type Device struct {
	ID          string  `json:"id,omitempty"`       // id of device
	Name        string  `json:"hostname"`           // name shown for this device
	IP          *string `json:"ip"`                 // ip address of device, and configured in the device - not addresses associated with this device
	Type        string  `json:"type"`               // category of device - categories are not exposed so we don't know the name of this type
	Description *string `json:"description"`        // a description of this device
	Sections    string  `json:"sections"`           // what sections have access to this device
	Location    *string `json:"location,omitempty"` // Where the device is located
}

// GetObjectID returns the object ID for this device
func (d Device) GetObjectID() string {
	return d.ID
}

func (d Device) GetObjectMetaData() ObjectMetadata {
	return ObjectMetadata{
		BasePath:   DevicesPath,
		DeleteOnId: true,
	}
}

// CopyFrom copies data from src that are present - empty strings are ignored
// returns true if any content was changed
func (d *Device) CopyFrom(src Device) bool {
	changed := false
	if d == nil {
		return false
	}
	if HasChangedStrPtr(&src.ID, &d.ID, false) {
		d.ID = src.ID
		changed = true
	}
	if HasChangedStrPtr(&src.Type, &d.Type, false) {
		changed = true
		d.Type = src.Type
	}
	if HasChangedStrPtr(src.IP, d.IP, false) {
		d.IP = src.IP
		changed = true
	}
	if HasChangedStrPtr(&src.Name, &d.Name, false) {
		d.Name = src.Name
		changed = true
	}
	if HasChangedStrPtr(&src.Sections, &d.Sections, false) {
		d.Sections = src.Sections
		changed = true
	}
	if HasChangedStrPtr(src.Description, d.Description, true) {
		d.Description = src.Description
		changed = true
	}
	if HasChangedStrPtr(src.Location, d.Location, true) {
		changed = true
		d.Location = src.Location
	}
	return changed
}

// GetDevicesWithParam returns all devices that are configured, allowing you to specify your own parameters
func (c *Connection) GetDevicesWithParam(p Param) (result []Device, err error) {
	gr, err := c.GetObject(p, Device{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}

// GetDevices returns all devices that are configured
func (c *Connection) GetDevices() (result []Device, err error) {
	return c.GetDevicesWithParam(NewParam())
}

// GetDevicesByKeyword returns all devices that are configured
func (c *Connection) GetDevicesByKeyword(keyword string) (result []Device, err error) {
	p := NewParam()
	p.RequestSuffix = "search/" + keyword
	return c.GetDevicesWithParam(p)
}

// FindDevice looks through the device list for a match and returns it
func FindDevice(name string, input *[]Device) *Device {
	for _, v := range *input {
		if v.Name == name {
			return &v
		}
	}
	return nil
}
