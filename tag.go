package phpipam

// Tag is tags used on Address objects. The API does not support writing / updates to a tag.
type Tag struct {
	Id        string `json:"id"`
	Type      string `json:"type"`
	ShowTag   string `json:"showtag"`
	BgColor   string `json:"bgcolor"`
	FgColor   string `json:"fgcolor"`
	Compress  string `json:"compress"` // [ Yes, No ]
	UpdateTag string `json:"updateTag"`
}

func (t Tag) GetObjectMetaData() ObjectMetadata {
	return ObjectMetadata{BasePath: "addresses/tags", ReadOnly: true}
}

func (t Tag) GetObjectID() string {
	return t.Id
}

// GetAddressTags return all address tags used in address objects
func (c *Connection) GetAddressTags() (result []Tag, err error) {
	gr, err := c.GetObject(NewParam(), Tag{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}

// GetAddressesByTag returns all addresses that is associated with this tag.
func (c *Connection) GetAddressesByTag(t Tag) (result []Address, err error) {
	p := NewParam()
	p.RequestSuffix = t.GetObjectID() + "/addresses"
	gr, err := c.GetObject(p, Tag{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}

// GetTagById gets the Tag from phpIPAM based on the ID.
func (c *Connection) GetTagById(id string) (result Tag, err error) {
	p := NewParam()
	p.RequestSuffix = id
	gr, err := c.GetObject(p, Tag{})
	if err == nil {
		err = Interface2Object(gr.Data, &result)
	}
	return
}
