package phpipam

import "bitbucket.org/HelgeOlav/utils/version"

// Version version of package
const Version = "0.0.1.0"

func init() {
	version.AddModule(version.ModuleVersion{Name: "bitbucket.org/HelgeOlav/phpipam", Version: Version})
}
